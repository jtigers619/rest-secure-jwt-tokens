package org.javatigers.jaxrs.srv.cxf.config;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_TIME;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.owlike.genson.ext.jaxb.JAXBBundle;

import se.blinfo.genson.BigDecimalConverter;
import se.blinfo.genson.Java8TypesBundle;

/**
 * 
 * @author ad
 *
 */
public class GensonConverter {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static final Genson genson = new GensonBuilder()
			.useDateAsTimestamp(false)
			//.withConverterFactory(new DefaultConverters.EnumConverterFactory(false))
			.withConverter(BigDecimalConverter.instance, BigDecimal.class)
			.withBundle(
					new Java8TypesBundle()
					.useLocalDateFormatter(formatter)
					.useLocalDateTimeFormatter(ISO_LOCAL_DATE_TIME)
					.useLocalTimeFormatter(ISO_LOCAL_TIME)
					.useInstantFormatter(ISO_INSTANT)
					.useOffsetDateTimeFormatter(ISO_OFFSET_DATE_TIME)
					.useOffsetTimeFormatter(ISO_OFFSET_TIME)
					.useZonnedDateTimeFormatter(ISO_ZONED_DATE_TIME) 
					, new JAXBBundle()
					)
			.create();
	
	public static Genson gensonInstance () {
		return genson;
	}

}

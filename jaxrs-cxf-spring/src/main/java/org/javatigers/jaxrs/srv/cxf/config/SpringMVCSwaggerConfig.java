package org.javatigers.jaxrs.srv.cxf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Configuration for Swagger oprations.
 * 
 * @author amit.dhiman
 *
 */
@Configuration
@EnableWebMvc
public class SpringMVCSwaggerConfig extends WebMvcConfigurerAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/*").addResourceLocations(
				"classpath:/META-INF/resources/webjars/swagger-ui/2.1.4/");
	}
	
}

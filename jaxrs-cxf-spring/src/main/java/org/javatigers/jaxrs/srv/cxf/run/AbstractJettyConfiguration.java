package org.javatigers.jaxrs.srv.cxf.run;

public abstract class AbstractJettyConfiguration {
	
	private static final int HTTP_PORT_NO = 8080;
	private static final String HTTP_PORT = "HTTP_PORT";
	
	protected static int getHttpPort () {
		String port = System.getenv(HTTP_PORT);
		return port != null ?  Integer.parseInt(port) : HTTP_PORT_NO;
	}
}

package org.javatigers.jaxrs.srv.cxf.run;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.javatigers.jaxrs.srv.cxf.config.RootApplicationConfig;
import org.javatigers.jaxrs.srv.cxf.config.SpringMVCSwaggerConfig;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Kick off jetty server.
 * 
 * @author ad
 *
 */
public class JettyServer extends AbstractJettyConfiguration {

	public static void main(String[] args) throws Exception {
		jettyServer ();
	}
	
	private static void jettyServer () throws Exception {
		Server server = new Server(getHttpPort());
		final ServletContextHandler context = new ServletContextHandler();  
		context.setContextPath( "/jaxrs-cxf" );
		
		registerCXFServlet(context);
		registerSpringContextLoaderListener(context);
		registerSpringSecurityFilterChain(context);
		registerSwaggerSpringServlet(context);
		
		server.setHandler( context );
        server.start();
        server.join();
	}
	
	/**
	 * CXFServlet is for Processes api requests.
	 * 
	 * @param context
	 */
	private static void registerCXFServlet (final ServletContextHandler context) {
		// Register and map the dispatcher servlet
		final ServletHolder servletHolder = new ServletHolder( new CXFServlet() );
		context.addServlet( servletHolder, "/api/*" ); 
	}
	
	/**
	 * Bootstrap the root application context as usual using ContextLoaderListener.
	 * Configure ContextLoaderListener to use AnnotationConfigWebApplicationContext instead of the default XmlWebApplicationContext.
	 * Configuration locations must consist of one or more comma- or space-delimited fully-qualified @Configuration classes.
	 * 
	 * @param context
	 */
	private static void registerSpringContextLoaderListener (final ServletContextHandler context) {
		context.addEventListener( new ContextLoaderListener() );
		context.setInitParameter( "contextClass", AnnotationConfigWebApplicationContext.class.getName() );
		context.setInitParameter( "contextConfigLocation", RootApplicationConfig.class.getName() );

	}
	
	private static void registerSwaggerSpringServlet (final ServletContextHandler context) {
		final ServletHolder servletHolder = new ServletHolder( new DispatcherServlet() );
		servletHolder.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
		servletHolder.setInitParameter("contextConfigLocation", SpringMVCSwaggerConfig.class.getName());
		context.addServlet( servletHolder, "/swagger-ui/*" ); 

	}
	
	private static void registerSpringSecurityFilterChain (final ServletContextHandler context) {
		FilterHolder filterHolder = new FilterHolder(new DelegatingFilterProxy());
		filterHolder.setName("springSecurityFilterChain");
		context.addFilter(filterHolder, "/*",  getDispatcherTypes());

	}
	
	private static EnumSet<DispatcherType> getDispatcherTypes() {
		EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(
				DispatcherType.REQUEST, DispatcherType.FORWARD);
		//, DispatcherType.INCLUDE, DispatcherType.ERROR, DispatcherType.ASYNC
		return dispatcherTypes;
	}
}

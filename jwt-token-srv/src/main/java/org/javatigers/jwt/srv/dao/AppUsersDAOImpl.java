package org.javatigers.jwt.srv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.javatigers.jwt.srv.domain.AppUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * A jdbc implementation of {@link AppUsersDAO}.
 *
 * @author Amit Dhiman
 *
 */
@Repository
public class AppUsersDAOImpl implements AppUsersDAO {
	 // --- members ---

    private final JdbcOperations jdbcOperations;

    // --- constructors ---

    @Autowired
    public AppUsersDAOImpl(JdbcOperations jdbcOperations) {
    	Assert.notNull(jdbcOperations, "jdbcOperations cannot be null");
        this.jdbcOperations = jdbcOperations;
    }

    // --- AppUsersDao methods ---

    @Override
    @Transactional(readOnly = true)
    public AppUsers getUser(int id) {
        return jdbcOperations.queryForObject(APP_USERS_QUERY + "id = ?", APP_USERS_MAPPER, id);
    }

    @Override
    @Transactional(readOnly = true)
    public AppUsers findUserByEmail(String email) {
        if (email == null) {
            throw new IllegalArgumentException("email cannot be null");
        }
        try {
            return jdbcOperations.queryForObject(APP_USERS_QUERY + "email = ?", APP_USERS_MAPPER, email);
        } catch (EmptyResultDataAccessException notFound) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<AppUsers> findUsersByEmail(String email) {
        if (email == null) {
            throw new IllegalArgumentException("email cannot be null");
        }
        if ("".equals(email)) {
            throw new IllegalArgumentException("email cannot be empty string");
        }
        return jdbcOperations.query(APP_USERS_QUERY + "email like ? order by id", APP_USERS_MAPPER, email + "%");
    }

    @Override
    public int createUser(final AppUsers userToAdd) {
        if (userToAdd == null) {
            throw new IllegalArgumentException("userToAdd cannot be null");
        }
        if (userToAdd.getId() != null) {
            throw new IllegalArgumentException("userToAdd.getId() must be null when creating a "+AppUsers.class.getName());
        }
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.jdbcOperations.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        "insert into app_users (email, password, first_name, last_name) values (?, ?, ?, ?)",
                        new String[] { "id" });
                ps.setString(1, userToAdd.getEmail());
                ps.setString(2, userToAdd.getPassword());
                ps.setString(3, userToAdd.getFirstName());
                ps.setString(4, userToAdd.getLastName());
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

    // --- non-public static members ---

    private static final String APP_USERS_QUERY = "select id, email, password, first_name, last_name from app_users where ";

    private static final RowMapper<AppUsers> APP_USERS_MAPPER = new AppUsersRowMapper("app_users.");

    /**
     * Create a new RowMapper that resolves {@link AppUsers}'s given a column label prefix. By allowing the prefix
     * to be specified we can reuse the same {@link RowMapper} for joins in other tables.
     *
     * @author Rob Winch
     *
     */
    static class AppUsersRowMapper implements RowMapper<AppUsers> {
        private final String columnLabelPrefix;

        /**
         * Creates a new instance that allows for a custom prefix for the columnLabel.
         *
         * @param columnLabelPrefix
         */
        public AppUsersRowMapper(String columnLabelPrefix) {
            this.columnLabelPrefix = columnLabelPrefix;
        }

        public AppUsers mapRow(ResultSet rs, int rowNum) throws SQLException {
            AppUsers user = new AppUsers();
            user.setId(rs.getInt(columnLabelPrefix + "id"));
            user.setEmail(rs.getString(columnLabelPrefix + "email"));
            user.setPassword(rs.getString(columnLabelPrefix + "password"));
            user.setFirstName(rs.getString(columnLabelPrefix + "first_name"));
            user.setLastName(rs.getString(columnLabelPrefix + "last_name"));
            return user;
        }
    };
}

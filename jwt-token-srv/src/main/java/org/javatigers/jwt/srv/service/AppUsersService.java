package org.javatigers.jwt.srv.service;

import java.util.List;

import org.javatigers.jwt.srv.domain.AppUsers;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 * An interface for managing {@link AppUsers} instances.
 *
 * @author Amit Dhiman
 *
 */
public interface AppUsersService {
	
	/**
     * Gets a {@link AppUsers} for a specific {@link AppUsers#getId()}.
     *
     * @param id
     *            the {@link AppUsers#getId()} of the {@link AppUsers} to find.
     * @return a {@link AppUsers} for the given id. Cannot be null.
     * @throws EmptyResultDataAccessException
     *             if the {@link AppUsers} cannot be found
     */
    AppUsers getUser(int id);

    /**
     * Finds a given {@link AppUsers} by email address.
     *
     * @param email
     *            the email address to use to find a {@link AppUsers}. Cannot be null.
     * @return a {@link AppUsers} for the given email or null if one could not be found.
     * @throws IllegalArgumentException
     *             if email is null.
     */
    AppUsers findUserByEmail(String email);

    /**
     * Finds any {@link AppUsers} that has an email that starts with {@code partialEmail}.
     *
     * @param partialEmail
     *            the email address to use to find {@link AppUsers}s. Cannot be null or empty String.
     * @return a List of {@link AppUsers}s that have an email that starts with given partialEmail. The returned
     *         value will never be null. If no results are found an empty List will be returned.
     * @throws IllegalArgumentException
     *             if email is null or empty String.
     */
    List<AppUsers> findUsersByEmail(String partialEmail);

    /**
     * Creates a new {@link AppUsers}.
     *
     * @param user
     *            the new {@link AppUsers} to create. The {@link AppUsers#getId()} must be null.
     * @return the new {@link AppUsers#getId()}.
     * @throws IllegalArgumentException
     *             if {@link AppUsers#getId()} is non-null.
     */
    int createUser(AppUsers user);

}

package org.javatigers.jwt.srv.service;

import java.util.Collection;
import java.util.List;

import org.javatigers.jwt.srv.dao.AppUsersDAO;
import org.javatigers.jwt.srv.domain.AppUsers;
import org.javatigers.jwt.srv.utils.UsersAuthorityUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Integrates with Spring Security using our existing {@link AppUsersDao} by looking up a {@link AppUsers} and
 * converting it into a {@link UserDetails} so that Spring Security can do the username/password comparison for us.
 *
 * @author Amit Dhiman
 * @see AppUsersAuthenticationProvider
 */
@Service("appUsersService")
public class AppUsersServiceImpl implements AppUsersService, UserDetailsService {
	
	private final AppUsersDAO appUsersDAO;
	
	public AppUsersServiceImpl (AppUsersDAO appUsersDAO) {
		Assert.notNull(appUsersDAO, "AppUsersDao cannot be null");
		this.appUsersDAO = appUsersDAO;
	}
	
	 /**
     * Lookup a {@link AppUsers} by the username representing the email address. Then, convert the
     * {@link AppUsers} into a {@link UserDetails} to conform to the {@link UserDetails} interface.
     */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUsers appUsers = appUsersDAO.findUserByEmail(username);
		if (null == appUsers) {
			throw new UsernameNotFoundException("Invalid username/password.");
		}
		return new AppUsersDetails(appUsers);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public AppUsers getUser(int id) {
		return appUsersDAO.getUser(id);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public AppUsers findUserByEmail(String email) {
		return appUsersDAO.findUserByEmail(email);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AppUsers> findUsersByEmail(String partialEmail) {
		return appUsersDAO.findUsersByEmail(partialEmail);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int createUser(AppUsers user) {
		return appUsersDAO.createUser(user);
	}
	
	/**
     * There are advantages to creating a class that extends {@link AppUsers}, our domain notion of a user, and
     * implements {@link UserDetails}, Spring Security's notion of a user.
     * <ul>
     * <li>First we can obtain all the custom information in the {@link AppUsers}</li>
     * <li>Second, we can use this service to integrate with Spring Security in other ways (i.e. when implementing
     * Spring Security's <a
     * href="http://static.springsource.org/spring-security/site/docs/3.1.x/reference/remember-me.html">Remember-Me
     * Authentication</a></li>
     * </ul>
     *
     * @author Amit Dhiman
     *
     */
    private final class AppUsersDetails extends AppUsers implements UserDetails {
        AppUsersDetails(AppUsers user) {
            setId(user.getId());
            setEmail(user.getEmail());
            setFirstName(user.getFirstName());
            setLastName(user.getLastName());
            setPassword(user.getPassword());
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return UsersAuthorityUtils.createAuthorities(this);
        }

        @Override
        public String getUsername() {
            return getEmail();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        private static final long serialVersionUID = 3384436451564509032L;
    }
}

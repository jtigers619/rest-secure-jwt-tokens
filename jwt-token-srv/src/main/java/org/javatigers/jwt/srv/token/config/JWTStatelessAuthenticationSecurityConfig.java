package org.javatigers.jwt.srv.token.config;

import org.javatigers.jwt.srv.token.core.JWTTokenHandler;
import org.javatigers.jwt.srv.token.core.authentication.JWTAuthenticationProvider;
import org.javatigers.jwt.srv.token.core.filters.JWTStatelessAuthenticationFilter;
import org.javatigers.jwt.srv.token.core.filters.JWTUsernamePasswordAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
//@Configuration
@Import(SpringConfig.class)
@Order(1)
public class JWTStatelessAuthenticationSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private JWTAuthenticationProvider jwtAuthenticationProvider;
	
	@Autowired
	private JWTTokenHandler jwtTokenHandler;
	
	public JWTStatelessAuthenticationSecurityConfig() {
		super(true);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.exceptionHandling().and()
				.anonymous()
				.and()
				.servletApi()
				.and()
				.headers().cacheControl()
				.and()
				.and()
				.authorizeRequests()
								
				//allow anonymous resource requests
				.antMatchers("/").permitAll()
				.antMatchers("/favicon.ico").permitAll()
				.antMatchers("/resources/**").permitAll()
				
				//allow anonymous POSTs to login
				.antMatchers(HttpMethod.POST, "/api/v1/login").permitAll()
				.antMatchers("/api/v1/**").hasRole("USER")
				//allow anonymous GETs to API
				//.antMatchers(HttpMethod.GET, "/api/v1/**").permitAll()
				
				//defined Admin only API area
				.antMatchers("/admin/**").hasRole("ADMIN")
				
				//all other request need to be authenticated
				.anyRequest().hasRole("USER").and()				
		
				// custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} which sets the token header upon authentication
				.addFilterBefore(new JWTUsernamePasswordAuthenticationFilter("/api/v1/login", authenticationManager(), jwtTokenHandler), UsernamePasswordAuthenticationFilter.class)

				// custom Token based authentication based on the header previously given to the client
				.addFilterBefore(new JWTStatelessAuthenticationFilter(jwtTokenHandler), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(jwtAuthenticationProvider);
	}

}

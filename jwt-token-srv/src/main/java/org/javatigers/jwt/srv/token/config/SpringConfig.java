package org.javatigers.jwt.srv.token.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
@ComponentScan(value = { "org.javatigers.jwt.srv" }, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "org.javatigers.jwt.srv.token.config.*"))
@Configuration
public class SpringConfig {
	
	@Bean(name = "h2DataSource")
	public DataSource h2DataSource() {

		// no need shutdown, EmbeddedDatabaseFactoryBean will take care of this
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2) // .H2 or
																		// .DERBY
				.addScript("classpath:/database/h2/users-schema.sql")
				.addScript("classpath:/database/h2/users-data.sql")
				.build();
		return db;
	}

	// Start WebServer, access http://localhost:8082
	/*@Bean(initMethod = "start", destroyMethod = "stop")
	public Server startDBManager() throws SQLException {
		return Server.createWebServer();
	}*/
	
	@Bean
	public JdbcTemplate getJdbcTemplate(@Qualifier("h2DataSource") DataSource h2DataSource) {
		return new JdbcTemplate(h2DataSource);
	}
}

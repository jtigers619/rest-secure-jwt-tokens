package org.javatigers.jwt.srv.token.constants;

public interface AppConstants {
	String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
}

package org.javatigers.jwt.srv.token.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.javatigers.jwt.srv.token.errors.Errors;
import org.javatigers.jwt.srv.utils.JacksonJSONUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.ExceptionTranslationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Used by {@link ExceptionTranslationFilter} to commence an authentication scheme.
 * 
 * @author ad
 *
 */
public class JWTRSAuthenticationEntryPoint implements AuthenticationEntryPoint {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
//		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        ObjectMapper mapper = JacksonJSONUtils.objectMapper();
        mapper.writeValue(response.getWriter(), Errors.of("Unauthorized", HttpStatus.UNAUTHORIZED));
	}

}

package org.javatigers.jwt.srv.token.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.javatigers.jwt.srv.token.errors.AuthenticationError;
import org.javatigers.jwt.srv.token.errors.ErrorCode;
import org.javatigers.jwt.srv.token.errors.Errors;
import org.javatigers.jwt.srv.utils.JacksonJSONUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * <tt>AuthenticationFailureHandler</tt> which performs a redirect to the value of the
 * {@link #setDefaultFailureUrl defaultFailureUrl} property when the
 * <tt>onAuthenticationFailure</tt> method is called. If the property has not been set it
 * will send a 401 response to the client, with the error message from the
 * <tt>AuthenticationException</tt> which caused the failure.
 * <p>
 * If the {@code useForward} property is set, a {@code RequestDispatcher.forward} call
 * will be made to the destination instead of a redirect.
 * 
 * @author ad
 *
 */
public class JWTRSAuthenticationFailureHandler implements AuthenticationFailureHandler {

	public JWTRSAuthenticationFailureHandler() {
	}

	/**
	 * Performs the redirect or forward to the {@code defaultFailureUrl} if set, otherwise
	 * returns a 401 error code.
	 * <p>
	 * If redirecting or forwarding, {@code saveException} will be called to cache the
	 * exception for use in the target view.
	 */
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

			response.setStatus(HttpStatus.UNAUTHORIZED.value());
	        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
	        ObjectMapper mapper = JacksonJSONUtils.objectMapper();
	        if (exception instanceof BadCredentialsException) {
	            mapper.writeValue(response.getWriter(), AuthenticationError.of("Unauthorized", ErrorCode.AUTHENTICATION));
	        } else if (exception instanceof AuthenticationServiceException) {
	        	 mapper.writeValue(response.getWriter(), Errors.of(exception.getMessage(), HttpStatus.UNAUTHORIZED));
	        }

	        mapper.writeValue(response.getWriter(), AuthenticationError.of("Authentication failed", ErrorCode.AUTHENTICATION));
	}

}

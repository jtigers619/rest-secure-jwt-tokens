package org.javatigers.jwt.srv.token.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.javatigers.jwt.srv.domain.AppUsers;
import org.javatigers.jwt.srv.utils.JacksonJSONUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * 
 * @author ad
 *
 */
public class JWTRSAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
			AppUsers userContext = (AppUsers) authentication.getPrincipal();
	        
	        response.setStatus(HttpStatus.OK.value());
	        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
	        JacksonJSONUtils.objectMapper().writeValue(response.getWriter(), getAppUserValuesMap(userContext));
	        clearAuthenticationAttributes(request);
	    }

	    /**
	     * Removes temporary authentication-related data which may have been stored
	     * in the session during the authentication process..
	     * 
	     */
	    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
	        HttpSession session = request.getSession(false);

	        if (session == null) {
	            return;
	        }

	        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	    }
	    
	    /**
	     * Json map
	     * 
	     * @param userContext
	     * @return Map<String, String>
	     */
	    private Map<String, String> getAppUserValuesMap (AppUsers userContext) {
	    	Map<String, String> userMap = new HashMap<>();
	    	userMap.put("email", userContext.getEmail());
	    	userMap.put("jwt", userContext.getJwtToken());
	    	return userMap;
	    }
}

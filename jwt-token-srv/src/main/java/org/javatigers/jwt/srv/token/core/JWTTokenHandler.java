package org.javatigers.jwt.srv.token.core;

import org.javatigers.jwt.srv.domain.AppUsers;
import org.javatigers.jwt.srv.service.AppUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component("jwtTokenHandler")
public final class JWTTokenHandler {

    private final String secret;
    private final AppUsersService appUsersService;
    
    @Autowired
    public JWTTokenHandler(@Value("${app.secret}") String secret, AppUsersService appUsersService) {
        this.secret = secret;
        this.appUsersService = appUsersService;
    }

    public AppUsers parseUserFromToken(String token) {
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        AppUsers user = appUsersService.findUserByEmail(username);
        if (user != null) {
        	return user;
        }
        return null;
    }

    public String createTokenForUser(AppUsers user) {
        return Jwts.builder()
                .setSubject(user.getEmail())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
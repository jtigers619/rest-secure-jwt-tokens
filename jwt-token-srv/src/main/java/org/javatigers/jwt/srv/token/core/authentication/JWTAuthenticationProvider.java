package org.javatigers.jwt.srv.token.core.authentication;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.javatigers.jwt.srv.domain.AppUsers;
import org.javatigers.jwt.srv.service.AppUsersService;
import org.javatigers.jwt.srv.service.AppUsersServiceImpl;
import org.javatigers.jwt.srv.utils.UsersAuthorityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * A Spring Security {@link AuthenticationProvider} that uses our {@link CalendarService} for authentication. Compare
 * this to our {@link AppUsersDetailsService} which is called by Spring Security's {@link DaoAuthenticationProvider}.
 *
 * <p>
 * This implementation has updates that allow for an additional parameter named domain to be passed in. In a real world
 * application the service tier would likely require three distinct fields (username, password) but since we
 * are learning about Spring Security not services we reuse our existing service by concatenating the
 * <code>username</code> to form the email.
 * </p>
 *
 * @author AmitDhiman
 * @see AppUsersServiceImpl
 */
@Component("jwtAuthenticationProvider")
public class JWTAuthenticationProvider implements AuthenticationProvider {
	
	private final AppUsersService appUsersService;
	private UserDetailsChecker preAuthenticationCheck = new PreAuthenticationChecks();
	
	@Autowired
	public JWTAuthenticationProvider (AppUsersService appUsersService) {
		Assert.notNull(appUsersService, "appUsersService cannot be null");
		this.appUsersService = appUsersService;
	}
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        String userName = token.getName();
        preAuthenticationCheck.check(authentication);
        AppUsers user = userName == null ? null : appUsersService.findUserByEmail(userName);
        if(user == null) {
            throw new UsernameNotFoundException("Invalid username/password");
        }
        String password = user.getPassword();
        if(!password.equals(token.getCredentials())) {
            throw new BadCredentialsException("Invalid username/password");
        }
        Collection<? extends GrantedAuthority> authorities = UsersAuthorityUtils.createAuthorities(user);
        return new UsernamePasswordAuthenticationToken(user, password, authorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.equals(authentication);
	}
	
	/**
	 * Check username password should not be blank.
	 * 
	 * @author amit.dhiman
	 */
	private class PreAuthenticationChecks implements UserDetailsChecker {

		@Override
		public void check(Authentication authentication) {
			if (StringUtils.isEmpty(authentication.getName())
					|| StringUtils.isEmpty(authentication.getCredentials().toString())) {
				throw new BadCredentialsException("Username password should not be blank");
			}
		}
	}

	/**
	 * Check user details.
	 * 
	 * @author amit.dhiman
	 *
	 */
	private interface UserDetailsChecker {
		public void check(Authentication authentication);
	}
}

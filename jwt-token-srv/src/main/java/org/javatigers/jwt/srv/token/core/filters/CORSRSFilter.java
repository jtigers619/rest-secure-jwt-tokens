package org.javatigers.jwt.srv.token.core.filters;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 
 * @author ad
 *
 */
public class CORSRSFilter extends CorsFilter {

	public CORSRSFilter() {
		super(configurationSource());
	}

	private static UrlBasedCorsConfigurationSource configurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(Boolean.FALSE);
		config.addAllowedOrigin("*");
		config.setAllowedHeaders(getAllowedHeaders());
		config.setAllowedMethods(getAllowedMethods());
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}

	private static List<String> getAllowedHeaders() {
		return Arrays.asList("Content-Type", "Authorization", "X-Requested-With", "Content-Length", "Accept", "Origin",
				"User-Token", "jwt");
	}

	private static List<String> getAllowedMethods() {
		return Arrays.asList("GET", "PUT", "POST", "DELETE", "OPTIONS");
	}
}

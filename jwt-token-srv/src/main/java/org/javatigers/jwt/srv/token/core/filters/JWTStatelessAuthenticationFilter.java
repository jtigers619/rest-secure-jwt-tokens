package org.javatigers.jwt.srv.token.core.filters;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.javatigers.jwt.srv.domain.AppUsers;
import org.javatigers.jwt.srv.token.constants.AppConstants;
import org.javatigers.jwt.srv.token.core.JWTTokenHandler;
import org.javatigers.jwt.srv.utils.UsersAuthorityUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class JWTStatelessAuthenticationFilter extends GenericFilterBean {
	private final JWTTokenHandler jwtTokenHandler;
	
	public JWTStatelessAuthenticationFilter (JWTTokenHandler jwtTokenHandler) {
		this.jwtTokenHandler = jwtTokenHandler;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		SecurityContextHolder.getContext().setAuthentication(
				getAuthentication((HttpServletRequest) request));
		filterChain.doFilter(request, response); // always continue
	}
	
	public Authentication getAuthentication(HttpServletRequest request) {
		final String token = request.getHeader(AppConstants.AUTH_HEADER_NAME);
		if (token != null) {
			final AppUsers user = jwtTokenHandler.parseUserFromToken(token);
			if (user != null) {
				Collection<? extends GrantedAuthority> authorities = UsersAuthorityUtils.createAuthorities(user);
	            return new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
			}
		}
		return null;
	}

}

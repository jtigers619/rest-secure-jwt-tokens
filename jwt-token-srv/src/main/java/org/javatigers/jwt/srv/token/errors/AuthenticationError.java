package org.javatigers.jwt.srv.token.errors;

/**
 * 
 * @author ad
 *
 */
public class AuthenticationError {
    // General Error message
    private final String message;
    // Error code
    private ErrorCode errorCode;

    protected AuthenticationError(final String message, final ErrorCode errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }
    
    public static AuthenticationError of(final String message, final ErrorCode errorCode) {
        return new AuthenticationError(message, errorCode);
    }
    
    public String getMessage() {
        return message;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

}

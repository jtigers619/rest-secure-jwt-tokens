package org.javatigers.jwt.srv.token.errors;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 
 * @author ad
 *
 */
public enum ErrorCode {
    GLOBAL(2),

    AUTHENTICATION(1001), 
    JWT_TOKEN_EXPIRED(1002),
    ACCESS_DENIED(1003);
    
    private int errorCode;

    private ErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errorCode;
    }
}

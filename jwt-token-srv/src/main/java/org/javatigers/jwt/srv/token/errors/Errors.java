package org.javatigers.jwt.srv.token.errors;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author ad
 *
 */
public class Errors {
    // HTTP Response Status Code
    private HttpStatus status;

    // General Error message
    private final String message;

    protected Errors(final String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }
    
    public static Errors of(final String message, HttpStatus status) {
        return new Errors(message, status);
    }
    
    public Integer getStatus() {
        return status.value();
    }

    public String getMessage() {
        return message;
    }

}

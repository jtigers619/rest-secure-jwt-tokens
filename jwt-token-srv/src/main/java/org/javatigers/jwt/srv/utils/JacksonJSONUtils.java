package org.javatigers.jwt.srv.utils;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * 
 * @author ad
 *
 */
public final class JacksonJSONUtils {
	
	private JacksonJSONUtils () {}
	
	public static String serialize (Object object) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		StringWriter strWriter = new StringWriter();
		mapper.writeValue(strWriter, object);
		return strWriter.toString();
	}
	
	public static ObjectMapper objectMapper () {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		return mapper;
	}
}

create table app_users (
    id bigint identity,
    email varchar(256) not null unique,
    password varchar(256) not null,
    first_name varchar(256) not null,
    last_name varchar(256) not null
);